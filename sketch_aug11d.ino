#include <SoftwareSerial.h>
#define redLED 12
#define yellowLED 14

SoftwareSerial firstSerial(2, 15); // RX, TX

byte cmd[9] = {0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};
unsigned char response[9];

void setup() {
  Serial.begin(9600); //Open serial communications and wait for port to open
  firstSerial.begin(9600); //set the data rate for the SoftwareSerial port
  pinMode(redLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
}

void loop() {
  memset(response, 0, 9);
  firstSerial.write(cmd, 9);
  firstSerial.readBytes(response, 9);
  int i;
  byte crc = 0;
  for (i = 1; i < 8; i++) crc += response[i];
  crc = 255 - crc;
  crc++;

  if ( !(response[0] == 0xFF && response[1] == 0x86 && response[8] == crc) ) {
    Serial.println("CRC error: " + String(crc) + " / " + String(response[8]));
  } else {
    unsigned int responseHigh = (unsigned int) response[2];
    unsigned int responseLow = (unsigned int) response[3];
    unsigned int ppm = (256 * responseHigh) + responseLow;
    Serial.println(ppm);
    if (ppm > 1000) {
      digitalWrite(redLED, HIGH);
      digitalWrite(yellowLED, LOW);
    }
    else {
      digitalWrite(redLED, LOW);
      digitalWrite(yellowLED, HIGH);
    }
  }

 
  delay(1000);

}



